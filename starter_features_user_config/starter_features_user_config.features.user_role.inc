<?php
/**
 * @file
 * starter_features_user_config.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function starter_features_user_config_user_default_roles() {
  $roles = array();

  // Exported role: Administrator.
  $roles['Administrator'] = array(
    'name' => 'Administrator',
    'weight' => '2',
  );

  return $roles;
}
